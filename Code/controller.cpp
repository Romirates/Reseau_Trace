/**
 * @file controller.cpp
 * @brief Functions implementation of class Controller
**/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_set>
#include "controller.hpp"

using namespace std;


Controller::Controller(double threshold)
  : threshold_ice(threshold)
{}


void Controller::add_router(Router* router)
{
    routers.push_back(router);
}


void Controller::receive_suspects(const std::vector<Entry>& data)
{
    to_analyse = data;
}


void Controller::update()
{
    if(to_analyse.size())
    {
        Entry new_entry;
        std::unordered_set<std::string> has_send;
        unsigned total_freq = 0, total_nb_elem = 0;
        for(auto& entry : to_analyse)
        {
			if(has_send.find(entry.elem) == has_send.end())
            {
				for(auto router : routers)
                {
					if(router->get_id() != entry.router_id)
                    {
						new_entry = router->get_entry(entry.elem);
						total_freq += new_entry.freq;
						total_nb_elem += new_entry.vect_nb_elem;
                    }
                }
                //add the freq and nb elem of entry to the total
                total_freq += entry.freq;
                total_nb_elem += entry.vect_nb_elem;
                if(total_freq >= threshold_ice*total_nb_elem)
                {
                    entry.freq = total_freq;
                    entry.vect_nb_elem = total_nb_elem;
                    send_iceberg(entry.elem);
                    print_iceberg_warning(entry);
                    has_send.insert(entry.elem);
                    //for testing purposes
                    ice_detected.insert(entry.elem);
                }
                total_freq = 0;
                total_nb_elem = 0;
            }
        }
        to_analyse.clear();
    }
}


void Controller::send_iceberg(std::string item)
{
    for(auto router : routers)
    {
        router->receive_iceberg(item);
    }
}


void Controller::print_iceberg_warning(Entry ent) 
{
    std::cout<<ent<<endl;
}

