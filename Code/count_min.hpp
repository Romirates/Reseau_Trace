/**
 * @file count_min.hpp
 * @brief Class definition of Count_Min
**/

#pragma once

#include "hash_string.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <queue>

/**
 * @brief The Count_Min class is an implementation of the 
 * 		  Count_Min_Sketch algorithm based on pairwises independant
 * 		  functions.
 */ 
class Count_Min 
{
    public:
		Count_Min(double epsi, double sigma);
		void add_entry(const std::string& entry);
		void add_entries(const std::vector<std::string>& entries);
		//reset to 0
		void reset();
		//return the estimated freq of the given item
		unsigned estim_freq(const std::string& item);
		//return the height of the CM_sketch
		unsigned get_height() const;
		//return the width of the CM_sketch
		unsigned get_width() const;
		//return the element's number in the vector
		unsigned get_nb_elem() const;
		
    private:
		typedef std::vector<std::vector<unsigned>> unsigned_matrix;
		static constexpr unsigned maxbuffer = 1000;
		static constexpr unsigned max_elem_fifo = 2000;
		unsigned nb_elem;
		unsigned height;
		unsigned width;
		unsigned_matrix count_min;
		Hash_String hash_funs;
		std::queue<std::string> items;
};
