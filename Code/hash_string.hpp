/**
 * @file hash_string.hpp
 * @brief definition of the Hash_String class
 */

#pragma once
#include <vector>
#include <cstdint>
#include <string>
#include <iostream>

class Hash_String 
{
    public:
        //constructor
        Hash_String(unsigned nb_fun, unsigned max_buffer);
        
        //methodes
        std::vector<uint32_t> hash(const std::string& str);
          
        //class tools
        friend std::ostream& operator<<(std::ostream& os, const Hash_String& item);
        friend std::ostream& operator<<(std::ostream& os, const std::vector<uint32_t>& item);
        static void test_init_hash_buffers(unsigned nb_fun, unsigned max_buffer);
        static void test_hash_string(const std::string& str, unsigned nb_fun, unsigned max_buffer);
  
    private:
        //attributs
        typedef std::vector<std::vector<uint64_t>> int64_matrix;
        int64_matrix funs;
          
        //methodes
        std::vector<uint64_t> init_64b_vector(unsigned max_buffer);
};
