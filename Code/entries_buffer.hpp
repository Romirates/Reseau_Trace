/**
 * @file entries_buffer.hpp
 * @brief Class definition of Entries_buffer
**/

#pragma once
#include "entry.hpp"
#include <vector>


/**
 * @brief This class handle the buffers,
 *        used by the router.
 **/
class Entries_buffer
{
    public:
		Entries_buffer();
		/**
		 *@brief Constructor of the Entries_buffer class.
		 *@param length The length of the buffer, timer_set The timer of the buffer.
		**/
		Entries_buffer(size_t length, unsigned timer_set);
		/**
		 *@brief Function that an entry to the buffer.
		 *@param elem The entry to add
		 **/
		void add_entry(Entry elem);
		/**
		 *@brief Function that check if the buffer,
		 *       needs to be flushed
		 **/
		bool need_to_flush();
		/**
		 *@brief Function that decrement the buffer's timer
		 **/
		void timer_decr();
		/**
		 *@brief Function that flush the buffer.
		 **/
		std::vector<Entry> flush();
		
    private:
		unsigned max_length;
		unsigned timer_max;
		unsigned timer;
		std::vector<Entry> buffer_entries;
		//check if the buffer is full
		bool is_full();
		//check if the timer is set to 0
		bool is_time_end();
		//check if an Entry is already in the buffer
		bool is_in(Entry elem);
};

