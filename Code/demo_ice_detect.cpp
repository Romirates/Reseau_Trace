#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <string>
#include <fstream>
#include <cstdlib>
#include <unordered_map>
#include <unordered_set>
#include "controller.hpp"
#include "router.hpp"

using namespace std;
bool compare(pair<string,size_t> p1, pair<string, size_t> p2)
{
    return (p1.second > p2.second);
}

int main(int argc, char *argv[])
{
    if(argc == 4)
    {
        string file = argv[1];
        size_t nb_router = atoi(argv[2]);
        double threshold_ice = atof(argv[3]);
        size_t learning_cycle = 100;
        ifstream data_input(file, ios::in);
        if(data_input.is_open())
        {
            //for testings purpose
            vector<pair<string, size_t>> sorted_entries; 
            unordered_map<string, size_t> static_entries;
            unordered_set<string> ice_detected;
            unsigned nb_entries = 0;
            string input_str;
            cout<<"Begin: static analyse..."<<endl;
            while(getline(data_input, input_str))
            {
                ++nb_entries;
                ++static_entries[input_str];
            }

            for(auto& elem : static_entries)
            {
                pair<string, size_t> p(elem.first, elem.second);
                sorted_entries.push_back(p);
            }
            sort(sorted_entries.begin(), sorted_entries.end(), compare);
            cout<<"End: static analyse."<<endl;
            //return to the beginning of the file
            data_input.clear(); //maybe useless for C++11
            data_input.seekg(0, ios::beg);
            cout<<"Begin: Iceberg detection..."<<endl;
            vector<Router*> routers;

            //Build the controller
            Controller controller(threshold_ice);

            //Build the routers
            for(size_t i = 0; i< nb_router; ++i) {
                Router* router =  new Router(controller,
                                             threshold_ice, nb_router);
                routers.push_back(router);
                controller.add_router(router);
            }
            
            //each router read a portion of data
            for(size_t i = 0; i < learning_cycle; ++i)
            {
                for (auto& router : routers)
                {
                    router->receive_data(data_input);
                }
            }
          
            //Activate detection for each router
            for(auto& router : routers) {
                router->activate();
            }
          
            //now each router will fill theirs buffer and send suspects to the controller
            while(!data_input.eof())
            {
                for(auto& router : routers)
                {
                    router->receive_data(data_input);
                    router->update();
                    controller.update();
                }
            }
            cout<<"End: Iceberg detection."<<endl;

            //final printing (ugly fast writed script please don't look)
            if(controller.ice_detected.size())
            {
                unsigned first_max = (sorted_entries.size() < 10 )? sorted_entries.size() : 10,
                    freq;
                string was_detected,
                    item;
                cout<<"The "<<first_max<<" more frequent items in "<<file<<" with a total of "<<nb_entries<<" items"<<"\n\n"
                    <<"| Item |"<<"\t"<<"| Frequency |"<<"\t"<<"| Detected |"<<endl;
                for(size_t i = 0; i<first_max; ++i)
                {
                    item = sorted_entries[i].first;
                    freq = sorted_entries[i].second;
                    was_detected = (controller.ice_detected.find(item) != controller.ice_detected.end())? "yes" : "no";
                    cout<<item<<" |      | "<<freq<<" |      | "<<was_detected<<" |"<<endl;
                }
            }
        }
    }
    else
    {
        cout<<"Usage: ice_detect [file_name] [nb_router] [threshold]\n"<<endl;
    }
    return EXIT_SUCCESS;
}
