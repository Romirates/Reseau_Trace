/**
 * @file entry.hpp
 * @brief definition of the Entry structure
 */
 
#pragma once

#include <string>
#include <iostream>

struct Entry
{
    unsigned router_id;
    std::string elem;
    unsigned freq;
    unsigned vect_nb_elem;
    
    /**
     * @brief Comparator for equality between 2 entry
     * @param ent the entry to compare
     * @return true <=> entry1.elem == entry2.elem
     **/
    bool operator==(const Entry& ent) const;
};


//overloading of the ostream << operator for an entry
/**
 * @brief A function to push Entry data in a stream
 * @param os The stream in which we want to put the data
 * @param ent The entry to print
 * @return the stream given in parameter
 **/
std::ostream& operator<<(std::ostream& os, const Entry& ent);
