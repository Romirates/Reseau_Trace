/**
 * @file count_min.cpp
 * @brief Functions implementation of class Count_Min
**/

#include "count_min.hpp"
#include <vector>
#include <string>
#include <cstdint>
#include <iostream>
#include <cmath>
#include <climits>
#include <queue>


/**
 * @brief Constructor of Count_min class
 * 
 * @param epsi Parameter epsilon of algorithm Count_min_sketch
 * @param sigma Parameter sigma of algorithm Count_min_sketch
 * 
 * this function initialize the count_min algorithm
 */
Count_Min::Count_Min(double epsi, double sigma)
//note : in cpp the (,) operator have its
//left operand evaluated before its right one
//thus, the following code is correct 
    : nb_elem(0),
        height((unsigned) ceil(log(1.0/sigma))),
        width((unsigned) ceil(exp(1.0)/epsi)),
        count_min(height),
        hash_funs(height, maxbuffer)
{
    for(size_t i=0; i<height; ++i) 
    {
        std::vector<unsigned> vect(width);
        count_min[i] = vect;
    }
}


/*
 * @brief Add an items into the algorithm
 * 
 * @param entries the items to add in the algorithm
 * 
 * This function is used to apply the Count_Min_Sketch algorithm to a 
 * given string.
 * This implementation use a queue in order to avoid reseting CM-Sketch algorithm.
 */
void Count_Min::add_entry(const std::string& entry) 
{
    ++nb_elem;
    std::vector<uint32_t> hashs = hash_funs.hash(entry);
    if(items.size() == max_elem_fifo)
    {
        --nb_elem;
        std::vector<uint32_t> hashs = hash_funs.hash(items.front());
        items.pop();
        for(size_t i=0; i<height; ++i)
        {
            --count_min[i][hashs[i] % width];
        }
    }
    items.push(entry);
    for(size_t i=0; i<height; ++i) 
    {
        ++count_min[i][hashs[i] % width];
    }
}
    

/*
 * @brief Add all given items into the algorithm
 * 
 * @param entries the list of items to add in the algorithm
 * 
 * This function is used to apply the Count_Min_Sketch algorithm to a 
 * given set of data.
 */
void Count_Min::add_entries(const std::vector<std::string>& entries) 
{
    for(auto& str : entries) 
    {
        add_entry(str);
    }
}


/*
 * @brief Reset the algorithm with parameters given in constructor
 */
void Count_Min::reset() 
{
    for(auto& vect : count_min) 
    {
        vect.assign(width,0);
    }
}


/*
 * @brief Estimate the frequency of an item
 * 
 * @param item the item for wich we want to estimate the frequency.
 * 
 * @return the estimate frequency of the given item
 */
unsigned Count_Min::estim_freq(const std::string& item) 
{
    unsigned min = UINT_MAX, freq_value;
    std::vector<uint32_t> hashs = hash_funs.hash(item);
    for(size_t i=0; i<height; ++i) 
    {
        freq_value = count_min[i][hashs[i] % width];
        min = (freq_value < min)? freq_value : min;
    }
    return min;
}


/*
 * @brief Give the heigth of the count_min_Sketch
 * 
 * @return The heigth of the count_min_Sketch
 */
unsigned Count_Min::get_height() const
{
    return height;
}


/*
 * @brief Give the width of the count_min_Sketch
 * 
 * @return The width of the count_min Sketch
 */
unsigned Count_Min::get_width() const
{
    return width;
}


/*
 * @brief Give the number of items in this count_min_Sketch
 * 
 * @return The number of items in this count_min Sketch
 */
unsigned Count_Min::get_nb_elem() const
{
    return nb_elem;
}

