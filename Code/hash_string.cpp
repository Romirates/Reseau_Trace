/**
 * @file hash_string.cpp
 * @brief Functions implementation of class Hash_String
**/

#include "hash_string.hpp"
#include <cstdint>
#include <string>
#include <iostream>
#include <random>
#include <vector>
#include <ctime>

Hash_String::Hash_String(unsigned nb_fun, unsigned max_buffer) 
{
	for(size_t i=0; i<nb_fun; ++i) 
	{
		funs.push_back(Hash_String::init_64b_vector(max_buffer));
	}
}


std::vector<uint64_t> Hash_String::init_64b_vector(unsigned max_buffer) 
{
	static std::default_random_engine eng(std::time(0));
	static std::uniform_int_distribution<uint64_t> uni_int64;
	std::vector<uint64_t> buffer;
	
	for(size_t i=0; i<max_buffer; ++i) 
	{
		buffer.push_back(uni_int64(eng));
	}
	return buffer;
}


std::vector<uint32_t> Hash_String::hash(const std::string& str) 
{
	std::vector<uint32_t> vector_hash;
	
	for(auto& coef_vector : funs) 
	{
		uint64_t sum = coef_vector[0];
		
		for(size_t i=0; i<str.size(); ++i) 
		{
			sum += str[i] * coef_vector[i+1];
		}
		vector_hash.push_back(sum >> 32);
	}
	return vector_hash;
}


std::ostream& operator<<(std::ostream& os, const Hash_String& item) 
{
	for(auto& coef_vector : item.funs) 
	{
		for(auto& elem : coef_vector) 
		{
			os << elem << " ";
		}
		os << std::endl;
	}
	return os;
}


std::ostream& operator<<(std::ostream& os, 
			  const std::vector<uint32_t>& item) 
{
	for(size_t i = 0; i<item.size(); ++i) 
	{
		os <<"Hash n°"<<i+1<<" : "<<(unsigned)item[i]<<std::endl;
	}
	return os;
}


void Hash_String::test_init_hash_buffers(unsigned nb_fun, 
										 unsigned max_buffer) 
{
	Hash_String hash_funs(nb_fun, max_buffer);
	std::cout << hash_funs << std::endl;
}	


void Hash_String::test_hash_string(const std::string& str, 
								   unsigned nb_fun, unsigned max_buffer)
{
	Hash_String hash_funs(nb_fun, max_buffer);
	std::cout << hash_funs.hash(str) << std::endl;
}


/*
int main(int argc, char *argv[]) {
  std::cout<<"*************************\n"
           <<"*   Hash_String tests   *\n"
           <<"*************************\n\n"<<std::endl;
  std::cout<<"test_init_hash_buffers(5, 100) : \n"
           <<"Construct 5 pairwise independant universal functions\n"
           <<"and print the 32bits interger vector used for each functions\n"
           <<std::endl;
  Hash_String::test_init_hash_buffers(5,100);
  std::cout<<"test_hash_string(hostname.random.net,5,100) : \n"
           <<"Construct 5 pairwire independant universal functions\n"
           <<"and return 5 hash of the given string\n"
           <<std::endl;
  Hash_String::test_hash_string("hostname.random.net", 50, 100);
}
*/

