/**
 *@file router.hpp
 *@brief Class definition of a router
 **/
 
#pragma once
#include "entries_buffer.hpp"
#include "controller.hpp"
#include "count_min.hpp"
#include <utility>
#include <fstream>

class Controller;

/**
 *@brief The router analyse a portion of a stream, 
 *       summarize this stream with a CM-sketch algorithm,
 *       and send potential icebergs to a controller.
 *       The router also receive request from the controller.
 **/
class Router 
{
    public:
		Router(Controller& _controller, double threshold, unsigned total_router);
		/**
		 *@brief Function that update the router's buffers,
		 *       send the suspects to controller or decrement a timer for each buffer
		 */
		void update();
		
		void receive_iceberg(std::string item);
		/**
		 *@brief Function that read one entry from a stream
		 *@param strm A reference to a stream
		 **/
		void receive_data(std::ifstream& strm);
		/**
		 *@brief Function that build an entry from a given
		 *       string
		 *@param str A string 
		 **/
		Entry get_entry(std::string str);
		unsigned get_id();
		void reset_cm();
		void activate();
		void desactivate();
	  
    private:
		Controller& controller;
		Count_Min count_min;
		double threshold_ice;
		//determine if the router receive data and send request
		//or just receive data
		bool send_and_receive;
		unsigned nb_buffer;
		std::vector<Entries_buffer> buffers;
		unsigned r_div_threshold;
		//previously detected iceberg
		std::vector<std::string> icebergs;
		unsigned nb_icebergs;
		//next_id count the number of router created
		static size_t next_id;
		unsigned id;
		static constexpr double r = 0.5;
		static constexpr double epsi = 0.5;
};
