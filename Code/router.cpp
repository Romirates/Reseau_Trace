/**
 * @file router.cpp
 * @brief All Implementation of the router class.
 */
 
#include "router.hpp"
#include "count_min.hpp"
#include "controller.hpp"
#include "entry.hpp"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iterator>

using namespace std;

size_t Router::next_id = 0;

Router::Router(Controller& _controller, double threshold, unsigned total_router)
:controller(_controller), count_min(0.01,0.1), threshold_ice(threshold),
        send_and_receive(false), nb_buffer(ceil(log(total_router) + 1)), buffers(nb_buffer),
        r_div_threshold((unsigned)ceil(r/threshold)), icebergs(r_div_threshold), nb_icebergs(0),
        id(next_id++)
{
    for(size_t i = 1; i <= nb_buffer-1; ++i)
    {
        size_t n = (size_t)floor(1.0/( threshold_ice + ((1.0 - threshold_ice) / pow(2, i))));
        buffers[i-1] = Entries_buffer((size_t)ceil(n*r), 100);
    }
    buffers[nb_buffer-1] = Entries_buffer((size_t)ceil(floor(1/threshold)*r), 100);
}


void Router::update()
{
    std::vector<Entry> entries, flushed_data;
    for(auto& buffer : buffers)
    {
        if(buffer.need_to_flush())
        {
            flushed_data = buffer.flush();
            entries.insert(entries.end(), flushed_data.begin(), flushed_data.end());
        }
        else
        {
            buffer.timer_decr();
        }
    }
    controller.receive_suspects(entries);
}


void Router::receive_iceberg(std::string item)
{
    icebergs[nb_icebergs++ % r_div_threshold] = item;  
}


void Router::receive_data(ifstream& strm)
{
    string str;
    if(getline(strm, str)) {
        count_min.add_entry(str);
        if(send_and_receive && (find(icebergs.begin(), icebergs.end(), str) == icebergs.end()))
        {
            unsigned buffer_pos = 0;
            //construction of an entry
            
            Entry entry = get_entry(str);
            //if the item's frequency is greater that the threshold

            if(entry.freq >= threshold_ice*(double)entry.vect_nb_elem)
            {
                //search its corresponding buffer
                if(entry.freq <= ((threshold_ice + ((1-threshold_ice) / pow(2,(nb_buffer-1))) )
                                  * (double)entry.vect_nb_elem) )
                {
                    buffer_pos = nb_buffer-1;
                }
                else
                {
                    while(entry.freq <= ((threshold_ice + ((1-threshold_ice) / pow(2,(buffer_pos+1))))
                                         * (double)entry.vect_nb_elem) )
                    {
                        ++buffer_pos;
                    }
                }
                buffers[buffer_pos].add_entry(entry);
            }
        }
    }
}


unsigned Router::get_id() 
{
    return id;
}


void Router::reset_cm()
{
    count_min.reset();
}


Entry Router::get_entry(std::string str)
{
    Entry ent;
    ent.router_id = id;
    ent.elem = str;
    ent.freq = count_min.estim_freq(str);
    ent.vect_nb_elem = count_min.get_nb_elem();
    return ent;
}


void Router::activate()
{
    send_and_receive = true;
}


void Router::desactivate()
{
    send_and_receive = false;
}

