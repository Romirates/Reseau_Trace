/**
 *@file controller.hpp
 *@brief Class definition of Controller
**/

#pragma once
#include "router.hpp"
#include "entry.hpp"
#include <vector>
#include <string>
#include <unordered_set> //for testing purposes


/**
 *@brief The controller receive potential suspects from a list of router
 *       and handle the global detection of an iceberg  
**/
class Router;

class Controller 
{
    public:
		/**
		 *@brief Contructor of the controller.
		 *@param threshold A threshold of detection (Θ ∈(0,1])
		 *
		 **/
		Controller(double threshold);
		void add_router(Router* router);
		/**
		 *@Function that update the list of suspects to analyse.
		 *@param data A vector of entry
		 **/
		void receive_suspects(const std::vector<Entry>& data);
		/**
		 *@brief Function that detect icebergs from its list of suspects,
		 *       and send the real icebergs to its list of routers.
		 **/
		void update();
		//for testing purposes
		std::unordered_set<std::string> ice_detected;

    private:
		double threshold_ice;
		std::vector<Router*> routers;
		std::vector<Entry> to_analyse;
		/**
		 *@brief Function that send an iceberg id to each router
		 *@param item The iceberg id
		 **/
		void send_iceberg(std::string item);
		/**
		 *@brief Funtion that print an icebergs report to the standard output
		 *@param entry The iceberg entry
		 **/
		void print_iceberg_warning(Entry entry);

};
