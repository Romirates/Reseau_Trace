/**
 * @file entries_buffer.cpp
 * @brief Functions implementation of Entries_Buffer class
**/

#include "entries_buffer.hpp"
#include "entry.hpp"
#include <algorithm>
#include <vector>

Entries_buffer::Entries_buffer()
    :max_length(0), timer_max(0), timer(0), buffer_entries(0)
{}


Entries_buffer::Entries_buffer(size_t length, unsigned timer_set)
    : max_length(length), timer_max(timer_set), timer(timer_set),
      buffer_entries(0)
{}


void Entries_buffer::timer_decr()
{
    --timer;
}


bool Entries_buffer::need_to_flush()
{
    return is_full() || is_time_end();
}


void Entries_buffer::add_entry(Entry elem)
{
    if(!is_in(elem))
    {
        buffer_entries.push_back(elem);
    }
}


bool Entries_buffer::is_full()
{
    return buffer_entries.size() == max_length;
}


bool Entries_buffer::is_time_end() 
{
    return !timer;
}


bool Entries_buffer::is_in(Entry elem)
{
    bool is_in = false;
    for(auto& entry : buffer_entries)
    {
        is_in = is_in && (entry == elem);
    }
    return is_in;
}


std::vector<Entry> Entries_buffer::flush()
{
    std::vector<Entry> output(buffer_entries.size());
    std::move(buffer_entries.begin(), buffer_entries.end(), output.begin());
    timer = timer_max;
    buffer_entries.clear();
    return output;
}

