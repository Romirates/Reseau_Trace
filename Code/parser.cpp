/**
 * @file parser.cpp
 * @brief Contain all the parser system (Thsi system is totally 
 * 			independant)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

//Typedefs
void parser_EPA(const char* filename, const char* outfile);
void parser_SDSC(const char* filename, const char* outfile);
void parser_Calgary(const char* filename, const char* outfile);


/**
 * @brief The main function of the parser 
 */
int main()
{
    parser_EPA("traces/1epahttp.txt", "epa.txt");
    parser_SDSC("traces/2sdschttp2.txt", "sdsc.txt");
    parser_Calgary("traces/3Calgaryaccess_log.txt", "calgary.txt");
    return EXIT_SUCCESS;
}


/**
 * @brief A parser for files structured like 1epahttp.txt
 *
 * @param filename The name of file to opening
 * @param outfile The basename of the output file
 * 
 * In this description, a word is is a contigous part of text, without 
 * any blank space
 * 
 * File Structure Needed :
 * 1. The host who make the request (1 word)
 * 2. The timestamp at format [DD:HH:MM:SS]
 * 3. The keyword of the HTTP protocol (1 word)
 * 3. The filename of the wanted file (1 word)
 * 4. Other data (limited to 10000 caracters, and terminated by a '\n')
 * 
 */
void parser_EPA(const char* filename, const char* outfile)
{
    //Init file
    fstream out1(string("1_").append(outfile).c_str(), ios::out);
    fstream out2(string("2_").append(outfile).c_str(), ios::out);
    fstream src(filename, ios::in);

    if (out1.is_open() && out2.is_open() && src.is_open())
    {
        while (!src.eof())
        {
            string host;
            string buf;
            string adress;
            src >> host >> buf >> buf >> adress;
            src.ignore(10000, '\n');

            if(adress.size() > 0)
                adress.erase(0, 1);
                
            if (adress == "")
				adress = "/";

            out1 << host << endl;
            out2 << adress << endl;
        }
        out1.close();
        out2.close();
        src.close();
    }
    else
    {
        cerr << "Parser EPA : Error with opening/closing of file" << endl;
    }
}


/**
 * @brief A parser for files structured like 2sdschttp.txt
 *
 * @param filename The name of file to opening
 * @param outfile The basename of the output file
 * 
 * In this description, a word is is a contigous part of text, without 
 * any blank space
 * 
 * File Structure Needed :
 * 1. The host who make the request, terminated by ':' caracter (1 word)
 * 2. The timestamp at format DAY MON DD HH:MM:SS YYYY
 * 3. The filename of the wanted file between braces and terminated by 
 * 	  ':' caracter (1 word)
 * 4. Other data (limited to 10000 caracters, and terminated by a '\n')
 * 
 */
void parser_SDSC(const char* filename, const char* outfile)
{
    //Init file
    fstream out1(string("1_").append(outfile).c_str(), ios::out);
    fstream out2(string("2_").append(outfile).c_str(), ios::out);
    fstream src(filename, ios::in);

    if (out1.is_open() && out2.is_open() && src.is_open())
    {
        while (!src.eof())
        {
            string host;
            string buf;
            string adress;
            src >> host >> buf >> buf >> buf >> buf >> buf >> adress;
            src.ignore(10000, '\n');

            if (host.size() > 0)
                host.pop_back();

            if (adress.size() > 2)
            {
                adress.erase(0, 1);
                adress.pop_back();
                adress.pop_back();
            }
            if (adress == "")
				adress = "/";

            out1 << host << endl;
            out2 << adress << endl;
        }
        out1.close();
        out2.close();
        src.close();
    }
    else
    {
        printf("Parser SDSC : Error with opening/closing of file\n");
    }
}


/**
 * @brief A parser for files structured like 3Calgaryaccess_log.txt
 *
 * @param filename The name of file to opening
 * @param outfile The basename of the output file
 * 
 * In this description, a word is is a contigous part of text, without 
 * any blank space
 * 
 * File Structure Needed :
 * 1. The host who make the request (1 word)
 * 2. Unused data (2 word)
 * 2. The timestamp at format [dd/MON/yyyy:hh:mm:ss -GTM] (2 word)
 * 3. The keyword of the HTTP protocol (1 word)
 * 3. The filename of the wanted file (1 word)
 * 4. Other data (limited to 10000 caracters, and terminated by a '\n')
 * 
 */
void parser_Calgary(const char* filename, const char* outfile)
{
    //Init file
    fstream out1(string("1_").append(outfile).c_str(), ios::out);
    fstream out2(string("2_").append(outfile).c_str(), ios::out);
    fstream src(filename, ios::in);

    if (out1.is_open() && out2.is_open() && src.is_open())
    {
        while (!src.eof())
        {
            string host;
            string buf;
            string adress;
            src >> host >> buf >> buf >> buf >> buf >> buf >> adress;
            src.ignore(10000, '\n');

            out1 << host << endl;
            out2 << adress << endl;
        }
        out1.close();
        out2.close();
        src.close();
    }
    else
    {
        printf("Parser Calgary : Error with opening/closing of file\n");
    }
}

