/**
 * @file count_min_tests.cpp
 * @brief Main file for testing Coount_Min Algorithm.
**/

#include "count_min.hpp"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include <unordered_map>
#include <cmath>
#include <climits>


/**
 * @brief Function that extracts the items to be frequency
 * the file need to have one string per line
 *
 */
std::vector<std::string> parsing_file(std::string file_name, 
										unsigned nb_request);


/**
 *@brief Function that extract the real frequency vector
 */
std::unordered_map<std::string, size_t> create_real_vector
					(std::vector<std::string> parsing_data);


/**
 *@brief function that create and fill a vector of count_min data 
 * 		 structurs with a (espi, sigma) settings and the parsing_data 
 * 		 for each one.
 */
std::vector<Count_Min> create_count_min_vector
						(std::vector<std::string> parsing_data,
                        std::vector<std::pair<double,double>> settings);



void average(std::unordered_map<std::string, size_t> real_data, 
												Count_Min count);


void std_deviation(std::unordered_map<std::string, size_t> real_data, 
													Count_Min count);


void min_max(std::unordered_map<std::string, size_t> real_data, 
												Count_Min count);


void dimension(std::unordered_map<std::string, size_t> real_data, 
												Count_Min count);


std::vector<std::string> parsing_file(std::string file_name, 
										unsigned nb_request)
{
    std::ifstream parse_file(file_name);
    std::vector<std::string> entries;
    unsigned cpt = nb_request;
    if (!parse_file)
    {
        std::cout<<"Can't open the file"<<std::endl;
        return entries;
    }
    else
    {
        std::string entry;
        while(cpt && std::getline(parse_file, entry))
        {
            entries.push_back(entry);
            --cpt;
        }
        return entries;
    }
}


std::unordered_map<std::string, size_t> create_real_vector
								(std::vector<std::string> parsing_data)
{
    std::unordered_map<std::string, size_t> real_vector;
    for(auto& item : parsing_data) {
        ++real_vector[item];
    }
    return real_vector;
}


std::vector<Count_Min> create_count_min_vector
						(std::vector<std::string> parsing_data,
                         std::vector<std::pair<double,double>> settings)
{
    std::vector<Count_Min> count_min_vect;
    for(auto& p : settings)
    {
        Count_Min count_min(p.first, p.second);
        count_min.add_entries(parsing_data);
        count_min_vect.push_back(count_min);
    }
    return count_min_vect;
}


void average(std::unordered_map<std::string,size_t> real_data, 
										      Count_Min count)
{
    unsigned nb_entry = real_data.size(), no_entry = 1;
    long double r_average = 0, e_average = 0;
  
    for(auto& kv : real_data)
    {
        r_average += no_entry * ((long double)kv.second/(long double)nb_entry);
        e_average += no_entry * ((long double)count.estim_freq(kv.first)/
                                 (long double)nb_entry);
        ++no_entry;
    }
    std::cout << "Real average: " << r_average << "\n" << "Estimated average: "
			  << e_average << "\n" << std::endl;
}
  
  
void std_deviation(std::unordered_map<std::string, size_t> real_data, 
													Count_Min count)
{
    unsigned  nb_entry = real_data.size(), no_entry = 1;
    double r_average = 0, r2_average = 0, e_average = 0, e2_average = 0,
        r_variance = 0, e_variance = 0;
  
    for(auto& kv : real_data)
    {
        r_average += no_entry * ((double)kv.second/(double)nb_entry);
        e_average += no_entry *
            ((double)count.estim_freq(kv.first)/(double)nb_entry);
    
        ++no_entry;
    }
    for(auto& kv : real_data)
    {
        r2_average += (no_entry*no_entry) * ((double)kv.second/(double)nb_entry);
        e2_average += (no_entry*no_entry) *
            ((double)count.estim_freq(kv.first)/(double)nb_entry);
        ++no_entry;
    }
    r_variance = r2_average - (r_average * r_average);
    e_variance = e2_average - (e_average * e_average);
    std::cout << "Real standard deviation: " << sqrt(r_variance) << "\n"
              << "Estimated standard deviation: " << sqrt(e_variance) <<
              "\n" << std::endl;
}


void min_max(std::unordered_map<std::string, size_t> real_data, 
												Count_Min count)
{
    unsigned r_min = UINT_MAX, e_min = UINT_MAX,
			 r_max = 0, e_max = 0;
    for(auto& kv : real_data)
    {
        unsigned estim = count.estim_freq(kv.first);
        r_min = (kv.second < r_min)? kv.second : r_min;
        r_max = (kv.second > r_max)? kv.second : r_max;
        e_min = (estim < e_min)? estim : e_min;
        e_max = (estim > e_max)? estim : e_max;
    }
    std::cout << "Reel minimum frequence: " << r_min << "\n"
              << "Estimated minimum frequence: " << e_min << "\n\n"
              << "Reel maximum frequence: " << r_max << "\n"
              << "Estimated maximum frequence: " << e_max << "\n" 
			  << std::endl;
}


void dimension(std::unordered_map<std::string, size_t> real_data, 
												Count_Min count)
{
    std::cout << "Reel vector dimension: " << real_data.size() << "\n"
              << "Estimation vector dimension: " << count.get_width()
              << "\n" << std::endl;
}

int main(int argc, char *argv[])
{
    std::vector<std::string> str_data = parsing_file(argv[1], 2000);
    std::unordered_map<std::string, size_t> real_vector = 
										create_real_vector(str_data);
    std::vector<std::pair<double,double>> settings = 
		  {{0.01,0.9}, {0.01,0.7}, {0.01,0.5}, {0.01,0.3}, {0.01, 0.1}};
    std::vector<Count_Min> count_min_vect = 
							create_count_min_vector(str_data, settings);
    
    std::cout << "*********************************\n"
              << "*            Tests              *\n"
              << "*********************************\n"
              << "\n\n" << std::endl;
              
    for(size_t i = 0; i < count_min_vect.size(); ++i)
    {
        std::cout << "Settings:" << "\n"
                  << "epsi: " << settings[i].first << "\n"
                  << "sigma: " << settings[i].second << "\n"
                  << std::endl;
        average(real_vector, count_min_vect[i]);
        std_deviation(real_vector, count_min_vect[i]);
        min_max(real_vector, count_min_vect[i]);
        dimension(real_vector, count_min_vect[i]);
    }
}
  
