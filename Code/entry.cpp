/**
 * @file entry.cpp
 * @brief Functions definition for Entry Structure
 **/

#include "entry.hpp"
#include <iostream>
#include <string>

/**
 * @brief Comparator for an Entry object
 * 
 * @param ent The second entry to compare
 * 
 * @return True if the twice items are similars, False otherwise.
 *      This function compare the elem param of Entries.
 */
bool Entry::operator==(const Entry& ent) const
{
    return elem == ent.elem;
}

/**
 * @brief A function for showing an item
 * 
 * @param os the output stream
 * @param ent The entry to show
 * 
 * @return the output stream given.
 */
std::ostream& operator<<(std::ostream& os, const Entry& ent)
{
    os <<"Router id: "<<ent.router_id<<"\t"
        <<"Item: "<<ent.elem<<"\t"
        <<"Frequency: "<<ent.freq<<"\t"
        <<"m: "<<ent.vect_nb_elem<<std::endl;
    return os;
}
